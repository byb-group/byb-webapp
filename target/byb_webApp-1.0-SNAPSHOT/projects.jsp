<%@page import="com.byb.model.Project"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Enumeration"%>

<!DOCTYPE html>
<html>

    <head>
        <title>Category | Kickstars</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.css"/>
        <link rel="stylesheet" href="css/jquery.sidr.light.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--[if lte IE 7]>
        <link rel="stylesheet" href="css/ie7.css"/>
        <![endif]-->
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/ie8.css"/>
        <![endif]-->
        <link rel="stylesheet" href="css/responsive.css"/>
        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.sidr.min.js"></script>
        <script type="text/javascript" src="js/twitter.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>

    </head>
    <body>
        <% ArrayList<Project> projects = (ArrayList<Project>) session.getAttribute("projects");
            if (projects == null) {
                response.sendRedirect("/byb/ProjectListServlet");
            }%>

        <div id="wrapper">
            <header id="header">
                <div class="wrap-top-menu">
                    <div class="container_12 clearfix">
                        <div class="grid_12">
                            <nav class="top-menu">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active"><a href="/byb/index.jsp">Home</a></li>
                                    <li class="sep"></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/contact.jsp">Contact</a></li>
                                </ul>
                                <a id="btn-toogle-menu" class="btn-toogle-menu" href="#alternate-menu">
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                </a>
                                <div id="right-menu">
                                    <ul class="alternate-menu">
                                        <li><a href="/byb/index.jsp">Home</a></li>
                                        <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                        <li><a href="/byb/contact.jsp">Contact us</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="top-message clearfix">
                                <i class="icon iFolder"></i>
                                <span class="txt-message">Welcome to ByBo site.</span>
                                <div class="clear"></div>
                            </div>
                            <i id="sys_btn_toggle_search" class="icon iBtnRed make-right"></i>
                        </div>
                    </div>
                </div><!-- end: .wrap-top-menu -->
                <div class="container_12 clearfix">
                    <div class="grid_12 header-content">
                        <div id="sys_header_right" class="header-right">
                            <div class="account-panel">

                                <%if (session.getAttribute("user") != null) {%>
                                <a href="/byb/LoginServlet" class="btn btn-red ">My Bybo</a>
                                <%} else {%>
                                <a href="#" class="btn btn-red sys_show_popup_login">Register</a>
                                <a href="#" class="btn btn-black sys_show_popup_login">Login</a>
                                <%}%>
                            </div>
                            <div class="form-search">
                                <form action="#">
                                    <label for="sys_txt_keyword">
                                        <input id="sys_txt_keyword" class="txt-keyword" type="text" placeholder="Search projects"/>
                                    </label>
                                    <button class="btn-search" type="reset"><i class="icon iMagnifier"></i></button>
                                    <button class="btn-reset-keyword" type="reset"><i class="icon iXHover"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="header-left">
                            <h1 id="logo">
                                <a href="/byb/index.jsp"><img src="images/logo.png" alt="$SITE_NAME"/></a>
                            </h1>
                            <div class="main-nav clearfix">
                                <div class="nav-item">
                                    <a href="/byb/ProjectListServlet" class="nav-title">Discover</a>
                                    <p class="rs nav-description">Great Projects</p>
                                </div>
                                <span class="sep"></span>
                                <div class="nav-item">
                                    <a href="/byb/AddProjectServlet" class="nav-title">Start</a>
                                    <p class="rs nav-description">Your Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header><!--end: #header -->

            <div class="layout-2cols">
                <div class="content grid_9">
                    <div class="search-result-page">
                        <div class="top-lbl-val">
                            <h3 class="common-title">Discover / <span class="fc-orange"> <%=(String) session.getAttribute("category")%></span></h3>
                            <div class="count-result">
                                <span class="fw-b fc-black"><%=projects.size()%> </span> projects found in this category
                            </div>
                        </div>
                        <div class="list-project-in-category">

                            <div class="list-project"> 
                                <%
                                    for (Project project : projects) {%>
                                <div class="grid_3">


                                    <div class="project-short sml-thumb">
                                        <div class="top-project-info">
                                            <div class="content-info-short clearfix">
                                                <a href="#" class="thumb-img">
                                                    <img src="images/ex/th-292x204-1.jpg" alt="$TITLE">
                                                </a>
                                                <div class="wrap-short-detail">
                                                    <h3 class="rs acticle-title"><a class="be-fc-orange" href="/byb/ProjectServlet?projectid=<%=project.getProjectid()%>"><%=project.getProjectname()%></a></h3>
                                                    <p class="rs tiny-desc">by <a href="#" class="fw-b fc-gray be-fc-orange"><%=project.getUsername()%></a></p>
                                                    <p class="rs title-description"><%=project.getDescription()%></p>
                                                    <p class="rs project-location">
                                                        <i class="icon iLocation"></i>
                                                        <%=project.getLocation()%>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bottom-project-info clearfix">
                                            <div class="line-progress">
                                                <div class="bg-progress">
                                                    <span  style="width:<%=project.getPercentage()%>%"></span>
                                                </div>
                                            </div>
                                            <div class="group-fee clearfix">
                                                <div class="fee-item">
                                                    <p class="rs lbl">Funded</p>
                                                    <span class="val"><%=project.getPercentage()%>%</span>
                                                </div>
                                                <div class="sep"></div>
                                                <div class="fee-item">
                                                    <p class="rs lbl">Pledged</p>
                                                    <span class="val">
                                                        <%=project.getPledge()%>
                                                    </span>
                                                </div>
                                                <div class="sep"></div>
                                                <div class="fee-item">
                                                    <p class="rs lbl">Days Left</p>
                                                    <span class="val"><%=project.getDayLeft()%></span>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div> 

                                </div><!--end: .grid_3 > .project-short-->
                                <% }%>

                                <div class="clear"></div>
                            </div>
                        </div><!--end: .list-project-in-category -->
                        <p class="rs ta-c">
                            <a id="showmoreproject" class="btn btn-black btn-load-more" href="#">Show more projects</a>
                        </p>
                    </div><!--end: .search-result-page -->
                </div><!--end: .content -->
                <div class="sidebar grid_3">

                    <div class="left-list-category">
                        <h4 class="rs title-nav">Category</h4>
                        <ul class="rs nav nav-category">
                            <li class="active">
                                <a href="/byb/ProjectListServlet">
                                    All projects
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Art">
                                    Art
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li >
                                <a href="/byb/ProjectListServlet?category=Comics">
                                    Comics
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Design">
                                    Design
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Fashion">
                                    Fashion
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Film">
                                    Film
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Games">
                                    Games
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>

                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Music">
                                    Music
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Photography">
                                    Photography
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Publishing">
                                    Publishing
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Technology">
                                    Technology
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/byb/ProjectListServlet?category=Theater">
                                    Theater
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                        </ul>
                    </div><!--end: .left-list-category -->

                    <div class="left-list-category">
                        <h4 class="rs title-nav">Featured</h4>
                        <ul class="rs nav nav-category">

                            <li class="active">
                                <a href="#">
                                    Popular
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    Ending Soon
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Small Project
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Most Funded
                                    <span class="count-val"></span>
                                    <i class="icon iPlugGray"></i>
                                </a>
                            </li>

                        </ul>
                    </div><!--end: .left-list-category -->
                </div><!--end: .sidebar -->
                <div class="clear"></div>
            </div>

            <footer id="footer">

                <div class="copyright">
                    <div class="container_12">
                        <div class="grid_12">
                            <a class="logo-footer"><img src="images/logo-2.png" alt="$SITE_NAME"/></a>
                            <p class="rs term-privacy">
                                <a class="fw-b be-fc-orange" >Tunisia</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >INSAT</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >GL4</a>
                            </p>
                            <p class="rs ta-c fc-gray-dark site-copyright">By  Anis BARKAOUI  &   Mohamed AFDHAL </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </footer>

        </div>

        <div class="popup-common" id="sys_popup_common">
            <div class="overlay-bl-bg"></div>
            <div class="container_12 pop-content">
                <div class="grid_12 wrap-btn-close ta-r">
                    <i class="icon iBigX closePopup"></i>
                </div>
                <div class="grid_6 prefix_1">
                    <div class="form login-form">
                        <form method="post" action="/byb/RegisterServlet">
                            <h3 class="rs title-form">Register</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">New to ByBo?</h4>
                                <p class="rs">A ByBo account is required to continue.</p>
                                <div class="form-action">
                                    <label for="txt_name">
                                        <input id="txt_name" class="txt fill-width" type="text" name="username" placeholder="Enter username"/>
                                    </label>
                                    <div class="wrap-2col clearfix">
                                        <div class="col">
                                            <label for="txt_email">
                                                <input id="txt_email" class="txt fill-width" type="email" name="email" placeholder="Enter your e-mail address"/>
                                            </label>
                                            <label for="txt_re_email">
                                                <input id="txt_re_email" class="txt fill-width" type="email" placeholder="Re-enter your e-mail adress"/>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="txt_password">
                                                <input id="txt_password" class="txt fill-width" type="password" name="password" placeholder="Enter password"/>
                                            </label>
                                            <label for="txt_re_password">
                                                <input id="txt_re_password" class="txt fill-width" type="password" placeholder="Re-enter password"/>
                                            </label>
                                        </div>
                                    </div>
                                    <p class="rs pb10">By signing up, you agree to our <a href="#" class="fc-orange">terms of use</a> and <a href="#" class="fc-orange">privacy policy</a>.</p>
                                    <p class="rs ta-c">
                                        <button class="btn btn-red btn-submit" type="submit">Register</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="form login-form">
                        <form method="post" action="/byb/LoginServlet">
                            <h3 class="rs title-form">Login</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">Already Have an Account?</h4>
                                <p class="rs">Please log in to continue.</p>
                                <div class="form-action">
                                    <label for="txt_email_login">
                                        <input id="txt_email_login" name= "login" class="txt fill-width" type="text" placeholder="Enter your login"/>
                                    </label>
                                    <label for="txt_password_login">
                                        <input id="txt_password_login" name= "password" class="txt fill-width" type="password" placeholder="Enter password"/>
                                    </label>

                                    <label for="chk_remember" class="rs pb20 clearfix">
                                        <input id="chk_remember" type="checkbox" class="chk-remember"/>
                                        <span class="lbl-remember">Remember me</span>
                                    </label>
                                    <p class="rs ta-c pb10">
                                        <button class="btn btn-red btn-submit" type="submit">Login</button>
                                    </p>
                                    <p class="rs ta-c">
                                        <a href="#" class="fc-orange">I forgot my password</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-20585382-5', 'megadrupal.com');
            ga('send', 'pageview');

        </script>
    </body>

</html>