<!DOCTYPE html>
<html>

    <head>
        <title>Welcome to ByBo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.css"/>
        <link rel="stylesheet" href="css/jquery.sidr.light.css"/>
        <link rel="stylesheet" href="css/animate.min.css"/>
        <link rel="stylesheet" href="css/md-slider.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel="stylesheet" href="css/responsive.css"/>

        <script type="text/javascript" src="js/raphael-min.js"></script>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.touchwipe.min.js"></script>
        <script type="text/javascript" src="js/md_slider.min.js"></script>
        <script type="text/javascript" src="js/jquery.sidr.min.js"></script>
        <script type="text/javascript" src="js/twitter.min.js"></script>
        <script type="text/javascript" src="js/pie.js"></script>
        <script type="text/javascript" src="js/script.js"></script>

    </head>
    <body>
        <div id="wrapper">
            
            
            <header id="header">
                <div class="wrap-top-menu">
                    <div class="container_12 clearfix">
                        <div class="grid_12">
                            <nav class="top-menu">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active"><a href="/byb/index.jsp">Home</a></li>
                                    <li class="sep"></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/contact.jsp">Contact</a></li>
                                </ul>
                                <a id="btn-toogle-menu" class="btn-toogle-menu" href="#alternate-menu">
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                </a>
                                <div id="right-menu">
                                    <ul class="alternate-menu">
                                        <li><a href="/byb/index.jsp">Home</a></li>
                                        <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                        <li><a href="/byb/contact.jsp">Contact us</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="top-message clearfix">
                                <i class="icon iFolder"></i>
                                <span class="txt-message">Welcome to ByBo site.</span>
                                <div class="clear"></div>
                            </div>
                            <i id="sys_btn_toggle_search" class="icon iBtnRed make-right"></i>
                        </div>
                    </div>
                </div><!-- end: .wrap-top-menu -->
                <div class="container_12 clearfix">
                    <div class="grid_12 header-content">
                        <div id="sys_header_right" class="header-right">
                            <div class="account-panel">

                                <%if (session.getAttribute("user") != null) {%>
                                <a href="/byb/LoginServlet" class="btn btn-red ">My Bybo</a>
                                <%} else {%>
                                <a href="#" class="btn btn-red sys_show_popup_login">Register</a>
                                <a href="#" class="btn btn-black sys_show_popup_login">Login</a>
                                <%}%>
                            </div>
                            <div class="form-search">
                                <form action="#">
                                    <label for="sys_txt_keyword">
                                        <input id="sys_txt_keyword" class="txt-keyword" type="text" placeholder="Search projects"/>
                                    </label>
                                    <button class="btn-search" type="reset"><i class="icon iMagnifier"></i></button>
                                    <button class="btn-reset-keyword" type="reset"><i class="icon iXHover"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="header-left">
                            <h1 id="logo">
                                <a href="/byb/index.jsp"><img src="images/logo.png" alt="$SITE_NAME"/></a>
                            </h1>
                            <div class="main-nav clearfix">
                                <div class="nav-item">
                                    <a href="/byb/ProjectListServlet" class="nav-title">Discover</a>
                                    <p class="rs nav-description">Great Projects</p>
                                </div>
                                <span class="sep"></span>
                                <div class="nav-item">
                                    <a href="/byb/AddProjectServlet" class="nav-title">Start</a>
                                    <p class="rs nav-description">Your Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header><!--end: #header -->
            
            
            <div id="home-slider">
                <div class="md-slide-items md-slider" id="md-slider-1" data-thumb-width="105" data-thumb-height="70">
                    <div class="md-slide-item slide-0" data-timeout="6000">
                        <div class="md-mainimg"><img src="images/ex/th-slide0.jpg" alt=""></div>
                        <div class="md-objects">
                            <div class="md-object rs slide-title" data-x="20" data-y="38" data-width="480" data-height="30" data-start="700" data-stop="5500" data-easein="random" data-easeout="keep">
                                <p>Search Money for Your Creative Ideas?</p>
                            </div>
                            <div class="md-object rs slide-description" data-x="20" data-y="160" data-width="480" data-height="90" data-start="1400" data-stop="7500" data-easein="random" data-easeout="keep">
                                <!--desc -->             <p></p>
                            </div>
                            <div class="md-object rs" data-x="20" data-y="260" data-width="120" data-height="23" data-padding-top="9" data-padding-bottom="7" data-padding-left="10" data-padding-right="10" data-start="1800" data-stop="7500" data-easein="random" data-easeout="keep">
                                <a href="#" class="btn btn-gray">Learn more</a>
                            </div>
                            <div class="md-object" data-x="495" data-y="0" data-width="612" data-height="365" data-start="1800" data-stop="7500" data-easein="fadeInRight" data-easeout="keep" style=""><img src="images/ex/th-slide-man.png" alt="Search Money for Your Creative Ideas" width="612" height="365" /></div>
                        </div>
                    </div>
                    <div class="md-slide-item slide-1" data-timeout="6000">
                        <div class="md-mainimg"><img src="images/ex/th-slide1.jpg" alt=""></div>
                        <div class="md-objects">
                            <div class="md-object rs slide-title" data-x="20" data-y="188" data-width="390" data-height="30" data-start="700" data-stop="5500" data-easein="random" data-easeout="random">
                                <p>A creative engine</p>
                            </div>
                            <div class="md-object rs slide-description2" data-x="20" data-y="250" data-width="390" data-height="100" data-start="1400" data-stop="4500" data-easein="random" data-easeout="random">
                                <!--desc -->         <p></p>
                            </div>
                            <div class="md-object" data-x="454" data-y="44" data-width="327" data-height="268" data-start="1000" data-stop="5500" data-easein="random" data-easeout="random" style=""><img src="images/ex/slide1_1.png" alt="Responsive" width="327" height="268" /></div>
                            <div class="md-object" data-x="628" data-y="142" data-width="298" data-height="176" data-start="1600" data-stop="5100" data-easein="random" data-easeout="random" style=""><img src="images/ex/slide1_2.png" alt="Responsive" width="298" height="176" /></div>
                            <div class="md-object" data-x="837" data-y="169" data-width="119" data-height="149" data-start="2200" data-stop="4800" data-easein="random" data-easeout="random" style=""><img src="images/ex/slide1_3.png" alt="Responsive" width="119" height="149" /></div>
                            <div class="md-object" data-x="809" data-y="208" data-width="59" data-height="114" data-start="2800" data-stop="4500" data-easein="random" data-easeout="random" style=""><img src="images/ex/slide1_4.png" alt="Responsive" width="59" height="114" /></div>
                        </div>
                    </div>
                    <div class="md-slide-item slide-2" data-timeout="4000">
                        <div class="md-mainimg"><img src="images/ex/th-slide2.jpg" alt=""></div>
                        <div class="md-objects">
                            <div class="md-object slide-with-background" data-x="20" data-y="58" data-width="500" data-height="170" data-padding-top="30" data-padding-bottom="30" data-padding-left="35" data-padding-right="35" data-start="300" data-stop="3600" data-easein="random" data-easeout="keep">
                                <h2 class="rs slide-title">Start your project today</h2>
                                <!--desc -->           <p class="rs slide-description2"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end: #home-slider -->
            <div class="home-feature-category">
                <div class="container_12 clearfix">
                    <div class="grid_4 left-lst-category">
                        <div class="wrap-lst-category">
                            <h3 class="title-welcome rs">Welcome to ByBo</h3>
                            <!--desc -->          <p class="description rs"></p>
                            <nav class="lst-category">
                                <ul class="rs nav nav-category">
                                    <li>
                                        <a href="#">
                                            Art
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li >
                                        <a href="#">
                                            Comics
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Design
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Fashion
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Film
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Games
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Music
                                            <span class="count-val">**</span>
                                            <i class="icon iPlugGray"></i>
                                        </a>
                                    </li>
                                </ul>
                                <p class="rs view-all-category">
                                    <a href="/byb/ProjectListServlet" class="be-fc-orange">+ View all categories</a>
                                </p>
                            </nav><!--end: .lst-category -->
                        </div>
                    </div><!--end: .left-lst-category -->
                    <div class="grid_8 marked-category">
                        <div class="wrap-title clearfix">
                            <h2 class="title-mark rs">Project of the week: </h2>
                            <a href="category.html" class="count-project be-fc-orange">See all <span class="fw-b">24</span> popular projects</a>
                        </div>
                        <div class="box-marked-project project-short">
                            <div class="top-project-info">
                                <div class="content-info-short clearfix">
                                    <a href="#" class="thumb-img">
                                        <img src="images/ex/th-292x204-1.jpg" alt="$TITLE">
                                    </a>
                                    <div class="wrap-short-detail">
                                        <h3 class="rs acticle-title"><a class="be-fc-orange" href="project.html">Project #1: To Do !!!</a></h3>
                                        <p class="rs tiny-desc">by <a href="profile.html" class="fw-b fc-gray be-fc-orange">To Do !!!</a> in <span class="fw-b fc-gray">New York, NY</span></p>
                                        <p class="rs title-description">To Do !!!</p>
                                    </div>
                                    <p class="rs clearfix comment-view">
                                        <a href="#" class="fc-gray be-fc-orange">*** comments</a>
                                        <span class="sep">|</span>
                                        <a href="#" class="fc-gray be-fc-orange">*** views</a>
                                    </p>
                                </div>
                            </div><!--end: .top-project-info -->
                            <div class="bottom-project-info clearfix">
                                <div class="project-progress sys_circle_progress" data-percent="76">
                                    <div class="sys_holder_sector"></div>
                                </div>
                                <div class="group-fee clearfix">
                                    <div class="fee-item">
                                        <p class="rs lbl">Bankers</p>
                                        <span class="val">To Do !!!</span>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="fee-item">
                                        <p class="rs lbl">Pledged</p>
                                        <span class="val">To Do !!!</span>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="fee-item">
                                        <p class="rs lbl">Days Left</p>
                                        <span class="val">To Do !!!</span>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div><!--end: .marked-category -->
                    <div class="clear"></div>
                </div>
            </div><!--end: .home-feature-category -->
            <div class="home-popular-project">
                <div class="container_12">
                    <div class="grid_12 wrap-title">
                        <h2 class="common-title">Popular</h2>
                        <a class="be-fc-orange" href="category.html">View all</a>
                    </div>
                    <div class="clear"></div>
                    <div class="lst-popular-project clearfix">



                        <div class="grid_3">
                            <div class="project-short sml-thumb">
                                <div class="top-project-info">
                                    <div class="content-info-short clearfix">
                                        <a href="#" class="thumb-img">
                                            <img src="images/ex/th-292x204-1.jpg" alt="$TITLE">
                                        </a>
                                        <div class="wrap-short-detail">
                                            <h3 class="rs acticle-title"><a class="be-fc-orange" href="project.html">Project title</a></h3>
                                            <p class="rs tiny-desc">by <a href="profile.html" class="fw-b fc-gray be-fc-orange">To Do !!!</a></p>
                                            <p class="rs title-description">To Do !!.</p>
                                            <p class="rs project-location">
                                                <i class="icon iLocation"></i>
                                                To Do !!!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-project-info clearfix">
                                    <div class="line-progress">
                                        <div class="bg-progress">
                                            <span  style="width: 50%"></span>
                                        </div>
                                    </div>
                                    <div class="group-fee clearfix">
                                        <div class="fee-item">
                                            <p class="rs lbl">Funded</p>
                                            <span class="val">To Do !!!</span>
                                        </div>
                                        <div class="sep"></div>
                                        <div class="fee-item">
                                            <p class="rs lbl">Pledged</p>
                                            <span class="val">To Do !!!</span>
                                        </div>
                                        <div class="sep"></div>
                                        <div class="fee-item">
                                            <p class="rs lbl">Days Left</p>
                                            <span class="val">To Do !!!</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end: .grid_3 > .project-short-->

                        <div class="clear clear-2col"></div>

                    </div>
                </div>
            </div><!--end: .home-popular-project -->



            <footer id="footer">

                <div class="copyright">
                    <div class="container_12">
                        <div class="grid_12">
                            <a class="logo-footer"><img src="images/logo-2.png" alt="$SITE_NAME"/></a>
                            <p class="rs term-privacy">
                                <a class="fw-b be-fc-orange" >Tunisia</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >INSAT</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >GL4</a>
                            </p>
                            <p class="rs ta-c fc-gray-dark site-copyright">By  Anis BARKAOUI  &   Mohamed AFDHAL </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </footer>

        </div>


        <div class="popup-common" id="sys_popup_common">
            <div class="overlay-bl-bg"></div>
            <div class="container_12 pop-content">
                <div class="grid_12 wrap-btn-close ta-r">
                    <i class="icon iBigX closePopup"></i>
                </div>
                <div class="grid_6 prefix_1">
                    <div class="form login-form">
                        <form method="post" action="/byb/RegisterServlet">
                            <h3 class="rs title-form">Register</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">New to ByBo?</h4>
                                <p class="rs">A ByBo account is required to continue.</p>
                                <div class="form-action">
                                    <label for="txt_name">
                                        <input id="txt_name" class="txt fill-width" type="text" name="username" placeholder="Enter username"/>
                                    </label>
                                    <div class="wrap-2col clearfix">
                                        <div class="col">
                                            <label for="txt_email">
                                                <input id="txt_email" class="txt fill-width" type="email" name="email" placeholder="Enter your e-mail address"/>
                                            </label>
                                            <label for="txt_re_email">
                                                <input id="txt_re_email" class="txt fill-width" type="email" placeholder="Re-enter your e-mail adress"/>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="txt_password">
                                                <input id="txt_password" class="txt fill-width" type="password" name="password" placeholder="Enter password"/>
                                            </label>
                                            <label for="txt_re_password">
                                                <input id="txt_re_password" class="txt fill-width" type="password" placeholder="Re-enter password"/>
                                            </label>
                                        </div>
                                    </div>
                                    <p class="rs pb10">By signing up, you agree to our <a href="#" class="fc-orange">terms of use</a> and <a href="#" class="fc-orange">privacy policy</a>.</p>
                                    <p class="rs ta-c">
                                        <button class="btn btn-red btn-submit" type="submit">Register</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="form login-form">
                        <form method="post" action="/byb/LoginServlet">
                            <h3 class="rs title-form">Login</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">Already Have an Account?</h4>
                                <p class="rs">Please log in to continue.</p>
                                <div class="form-action">
                                    <label for="txt_email_login">
                                        <input id="txt_email_login" name= "login" class="txt fill-width" type="text" placeholder="Enter your login"/>
                                    </label>
                                    <label for="txt_password_login">
                                        <input id="txt_password_login" name= "password" class="txt fill-width" type="password" placeholder="Enter password"/>
                                    </label>

                                    <label for="chk_remember" class="rs pb20 clearfix">
                                        <input id="chk_remember" type="checkbox" class="chk-remember"/>
                                        <span class="lbl-remember">Remember me</span>
                                    </label>
                                    <p class="rs ta-c pb10">
                                        <button class="btn btn-red btn-submit" type="submit">Login</button>
                                    </p>
                                    <p class="rs ta-c">
                                        <a href="#" class="fc-orange">I forgot my password</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-20585382-5', 'megadrupal.com');
            ga('send', 'pageview');

        </script>
    </body>

</html>