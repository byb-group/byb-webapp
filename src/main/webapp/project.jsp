<%@page import="com.byb.model.Comment"%>
<%@page import="com.byb.model.Project"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Project | ByBo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.css"/>
        <link rel="stylesheet" href="css/jquery.sidr.light.css"/>
        <link rel="stylesheet" href="css/responsiveslides.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--[if lte IE 7]>
        <link rel="stylesheet" href="css/ie7.css"/>
        <![endif]-->
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/ie8.css"/>
        <![endif]-->
        <link rel="stylesheet" href="css/responsive.css"/>
        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="js/raphael-min.js"></script>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/responsiveslides.min.js"></script>
        <script type="text/javascript" src="js/jquery.sidr.min.js"></script>
        <script type="text/javascript" src="js/twitter.min.js"></script>
        <script type="text/javascript" src="js/pie.js"></script>
        <script type="text/javascript" src="js/script.js"></script>

    </head>
    <body>
        <% Project project = (Project) session.getAttribute("project");
            ArrayList<Comment> comments = (ArrayList<Comment>) session.getAttribute("comments");
            if (project == null) {
                response.sendRedirect("/byb/ProjectListServlet");
            }%>
        <div id="wrapper">
            <header id="header">
                <div class="wrap-top-menu">
                    <div class="container_12 clearfix">
                        <div class="grid_12">
                            <nav class="top-menu">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active"><a href="/byb/index.jsp">Home</a></li>
                                    <li class="sep"></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/contact.jsp">Contact</a></li>
                                </ul>
                                <a id="btn-toogle-menu" class="btn-toogle-menu" href="#alternate-menu">
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                </a>
                                <div id="right-menu">
                                    <ul class="alternate-menu">
                                        <li><a href="/byb/index.jsp">Home</a></li>
                                        <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                        <li><a href="/byb/contact.jsp">Contact us</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="top-message clearfix">
                                <i class="icon iFolder"></i>
                                <span class="txt-message">Welcome to ByBo site.</span>
                                <div class="clear"></div>
                            </div>
                            <i id="sys_btn_toggle_search" class="icon iBtnRed make-right"></i>
                        </div>
                    </div>
                </div><!-- end: .wrap-top-menu -->
                <div class="container_12 clearfix">
                    <div class="grid_12 header-content">
                        <div id="sys_header_right" class="header-right">
                            <div class="account-panel">

                                <%if (session.getAttribute("user") != null) {%>
                                <a href="/byb/LoginServlet" class="btn btn-red ">My Bybo</a>
                                <%} else {%>
                                <a href="#" class="btn btn-red sys_show_popup_login">Register</a>
                                <a href="#" class="btn btn-black sys_show_popup_login">Login</a>
                                <%}%>
                            </div>
                            <div class="form-search">
                                <form action="#">
                                    <label for="sys_txt_keyword">
                                        <input id="sys_txt_keyword" class="txt-keyword" type="text" placeholder="Search projects"/>
                                    </label>
                                    <button class="btn-search" type="reset"><i class="icon iMagnifier"></i></button>
                                    <button class="btn-reset-keyword" type="reset"><i class="icon iXHover"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="header-left">
                            <h1 id="logo">
                                <a href="/byb/index.jsp"><img src="images/logo.png" alt="$SITE_NAME"/></a>
                            </h1>
                            <div class="main-nav clearfix">
                                <div class="nav-item">
                                    <a href="/byb/ProjectListServlet" class="nav-title">Discover</a>
                                    <p class="rs nav-description">Great Projects</p>
                                </div>
                                <span class="sep"></span>
                                <div class="nav-item">
                                    <a href="/byb/AddProjectServlet" class="nav-title">Start</a>
                                    <p class="rs nav-description">Your Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header><!--end: #header -->

            <div class="layout-2cols">
                <div class="content grid_8">
                    <div class="project-detail">
                        <h2 class="rs project-title"><%=project.getProjectname()%></h2>
                        <p class="rs post-by">by <a href="#"><%=project.getUsername()%></a></p>
                        <div class="project-short big-thumb">
                            <div class="top-project-info">
                                <div class="content-info-short clearfix">
                                    <div class="thumb-img">
                                        <div class="rslides_container">
                                            <ul class="rslides" id="slider1">
                                                <li><img src="images/ex/th-552x411-2.jpg" alt=""></li>
                                                <li><img src="images/ex/th-552x411-1.jpg" alt=""></li>
                                                <li><img src="images/ex/th-552x411-2.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end: .top-project-info -->
                            <div class="bottom-project-info clearfix">
                                <div class="project-progress sys_circle_progress" data-percent="<%=project.getPercentage()%>">
                                    <div class="sys_holder_sector"></div>
                                </div>
                                <div class="group-fee clearfix">
                                    <div class="fee-item">
                                        <p class="rs lbl">Bankers</p>
                                        <span class="val"><%=project.getBackers().size()%></span>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="fee-item">
                                        <p class="rs lbl">Pledged</p>
                                        <span class="val"><%=project.getPledge()%></span>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="fee-item">
                                        <p class="rs lbl">Days Left</p>
                                        <span class="val"><%=project.getDayLeft()%></span>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="project-tab-detail tabbable accordion">
                            <ul class="nav nav-tabs clearfix">
                                <li class="active"><a href="#">About</a></li>
                                <li><a href="#" class="be-fc-orange">Backers</a></li>
                                <li><a href="#" class="be-fc-orange">Comments</a></li>
                                <li><a href="#" class="be-fc-orange">Questions</a></li>

                            </ul>
                            <div class="tab-content">
                                <div>
                                    <h3 class="rs alternate-tab accordion-label">About</h3>
                                    <div class="tab-pane active accordion-content">
                                        <div class="editor-content">
                                            <h3 class="rs title-inside">Project description </h3>
                                            <p><%=project.getDescription()%></p>
                                            <p>
                                                <img class="img-desc" src="images/ex/th-552x411-2.jpg" alt="$DESCRIPTION"/>
                                                <span class="img-label">Meeting</span>
                                            </p>
                                            <div class="social-sharing">
                                                <!-- AddThis Button BEGIN -->
                                                <div class="addthis_toolbox addthis_default_style">
                                                    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                                    <a class="addthis_button_tweet"></a>
                                                    <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                                                    <a class="addthis_counter addthis_pill_style"></a>
                                                </div>
                                                <script type="text/javascript" src="../../../s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
                                                <!-- AddThis Button END -->
                                            </div>
                                        </div>
                                        <div class="project-btn-action">
                                            <!--                                            <a class="btn big btn-red" href="#">Ask a question</a>
                                                                                        <a class="btn big btn-black" href="#">Add comment</a>-->
                                        </div>
                                    </div><!--end: .tab-pane(About) -->
                                </div>

                                <div>
                                    <h3 class="rs alternate-tab accordion-label">Backers</h3>
                                    <div class="tab-pane accordion-content">
                                        <div class="tab-pane-inside">

                                            <%
                                                for (String backer : project.getBackers().keySet()) {

                                            %>
                                            <div class="project-author pb20">
                                                <div class="media">
                                                    <a href="#" class="thumb-left">
                                                        <img src="images/ex/th-90x90-1.jpg" alt="$USER_NAME"/>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="rs pb10"><a href="#" class="be-fc-orange fw-b"><%=backer%></a></h4>
                                                        <p class="rs"><%=project.getBackers().get(backer)%></p>
                                                    </div>
                                                </div>
                                            </div><!--end: .project-author -->
                                            <%}%>
                                        </div>
                                        <div class="project-btn-action">
                                            <center><a class="btn btn-black" href="#">Back this project</a></center>
                                        </div>
                                    </div><!--end: .tab-pane(Backers) -->
                                </div>
                                <div>
                                    <h3 class="rs active alternate-tab accordion-label">Comments</h3>
                                    <div class="tab-pane accordion-content">
                                        <div class="box-list-comment">

                                            <%
                                            if(comments!=null){
                                                for (Comment comment : comments) {

                                            %>
                                            <div class="media comment-item">
                                                <a href="#" class="thumb-left">
                                                    <img src="images/ex/th-90x90-1.jpg" alt="$TITLE">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="rs comment-author">
                                                        <a href="#" class="be-fc-orange fw-b"><%=comment.getUsername()%></a>
                                                        <span class="fc-gray">say:</span>
                                                    </h4>
                                                    <p class="rs comment-content"> <%=comment.getComment()%> </p>
                                                    <p class="rs time-post"><%=comment.getComment_ts()%></p>
                                                </div>
                                            </div><!--end: .comment-item -->
                                            <%}}%>

                                            <div class="project-btn-action">
                                                <div class="wrapper-box box-post-comment">
                                                    <h4 class="rs title-box-outside">Post comment</h4>
                                                    <div class="clear"></div>
                                                    <div class="box-white">
                                                        <form action="/byb/AddCommentServlet" method="post">
                                                            <div class="form form-post-comment">
                                                                   
                                                                <div >
                                                                    <label for="comment">
                                                                        <textarea  id="txt_content_comment" cols="30" rows="10" class="txt fill-width" name="comment" placeholder="Your comment "></textarea>
                                                                    </label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <p class="rs ta-r clearfix">
                                                                    <span class="thanks">Thanks for submitting your comment!<i class="pick-right"></i></span>
                                                                    <input type="submit" class="btn btn-white btn-submit-comment" value="Submit">
                                                                </p>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--end: .tab-pane(Comments) -->
                                </div>

                                <div>
                                    <h3 class="rs alternate-tab accordion-label">Questions</h3>
                                    <div class="tab-pane accordion-content">
                                        <div class="tab-pane-inside">
                                            <div class="list-last-post">

                                                <div class="media other-post-item">
                                                    <a href="#" class="thumb-left">
                                                        <img src="images/ex/th-90x90-1.jpg" alt="$TITLE">
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="rs title-other-post">
                                                            <a href="#" class="be-fc-orange fw-b">ToDo !!!</a>
                                                        </h4>
                                                        <p class="rs fc-gray time-post pb10">Asked *** days ago</p>
                                                        <p class="rs description">ToDo !!!</p>
                                                    </div>
                                                </div><!--end: .other-post-item -->

                                            </div>
                                        </div>
                                        <div class="project-btn-action">
                                            <center><a class="btn btn-black" href="#">Add Question</a></center>
                                        </div>
                                    </div><!--end: .tab-pane(Questions) -->
                                </div></div>
                        </div><!--end: .project-tab-detail -->
                    </div>
                </div><!--end: .content -->
                <div class="sidebar grid_4">
                    <div class="project-runtime">
                        <div class="box-gray">
                            <div class="project-date clearfix">
                                <i class="icon iCalendar"></i>
                                <span class="val"><span class="fw-b">Launched: </span><%=project.getLaunched_date()%></span>
                            </div>
                            <div class="project-date clearfix">
                                <i class="icon iClock"></i>
                                <span class="val"><span class="fw-b">Funding ends: </span><%=project.getFunding_end_date()%></span>
                            </div>
                            <a class="btn btn-green btn-buck-project sys_show_popup_login" href="#">
                                <span class="lbl">Back This Project</span>
                                <span class="desc"><%=project.getMinimum_pledge()%> $ minimum pledge</span>
                            </a>
                            <p class="rs description">This project will only be funded if at least <%=project.getBudjet()%> $ .</p>
                        </div>
                    </div><!--end: .project-runtime -->
                    <div class="project-author">
                        <div class="box-gray">
                            <h3 class="title-box">Project by</h3>
                            <div class="media">
                                <a href="#" class="thumb-left">
                                    <img src="images/ex/th-90x90-1.jpg" alt="$USER_NAME"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="rs pb10"><a href="#" class="be-fc-orange fw-b"><%=project.getUsername()%></a></h4>
                                    <p class="rs"><%=project.getLocation()%></p>
                                </div>
                            </div>
                            <div class="author-action">
                                <a class="btn btn-red" href="#">Contact me</a>
                                <a class="btn btn-white" href="#">See full bybo</a>
                            </div>
                        </div>
                    </div><!--end: .project-author -->
                    <div class="clear clear-2col"></div>


                </div><!--end: .sidebar -->
                <div class="clear"></div>
            </div>


            <footer id="footer">

                <div class="copyright">
                    <div class="container_12">
                        <div class="grid_12">
                            <a class="logo-footer"><img src="images/logo-2.png" alt="$SITE_NAME"/></a>
                            <p class="rs term-privacy">
                                <a class="fw-b be-fc-orange" >Tunisia</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >INSAT</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >GL4</a>
                            </p>
                            <p class="rs ta-c fc-gray-dark site-copyright">By  Anis BARKAOUI  &   Mohamed AFDHAL </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </footer>

        </div>

        <div class="popup-common" id="sys_popup_common">
            <div class="overlay-bl-bg"></div>
            <div class="container_12 pop-content">
                <div class="grid_12 wrap-btn-close ta-r">
                    <i class="icon iBigX closePopup"></i>
                </div>

                <div  class="grid_4">
                    <div class="form login-form">
                        <form action="/byb/ProjectServlet" method="post">
                            <h3 class="rs title-form">Login</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">Back this project</h4>
                                <p class="rs">you have to be logged in to continue.</p>
                                <div class="form-action">
                                    <label for="amount">
                                        <input id="amount" class="txt fill-width" type="text" name="amount" placeholder="Enter your pledge amount"/>
                                    </label>
                                    <label for="reward">
                                        <input id="reward" class="txt fill-width" type="text" name="reward" placeholder="Enter your reward"/>
                                    </label>


                                    <p class="rs ta-c pb10">
                                        <button class="btn btn-red btn-submit" type="submit">Back project</button>
                                    </p>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-20585382-5', 'megadrupal.com');
            ga('send', 'pageview');

        </script>
    </body>

</html>