<%@page import="com.byb.model.Comment"%>
<%@page import="com.byb.model.User"%>
<%@page import="com.byb.model.Project"%>

<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>

    <!-- Mirrored from envato.megadrupal.com/html/kickstars/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:24:22 GMT -->
    <head>
        <title>Profile | Bybo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.css"/>
        <link rel="stylesheet" href="css/jquery.sidr.light.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--[if lte IE 7]>
        <link rel="stylesheet" href="css/ie7.css"/>
        <![endif]-->
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/ie8.css"/>
        <![endif]-->
        <link rel="stylesheet" href="css/responsive.css"/>
        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/raphael-min.js"></script>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.sidr.min.js"></script>
        <script type="text/javascript" src="js/twitter.min.js"></script>
        <script type="text/javascript" src="js/pie.js"></script>
        <script type="text/javascript" src="js/script.js"></script>

    </head>
    <body>
        <% User user = (User) session.getAttribute("user");
            ArrayList<Project> userProjects = (ArrayList<Project>) session.getAttribute("userProjects");
            ArrayList<Comment> comments = (ArrayList<Comment>) session.getAttribute("comments");

            if (user == null) {
                response.sendRedirect("/byb/index.jsp");
            }%>
        <div id="wrapper">
            <header id="header">
                <div class="wrap-top-menu">
                    <div class="container_12 clearfix">
                        <div class="grid_12">
                            <nav class="top-menu">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active"><a href="/byb/index.jsp">Home</a></li>
                                    <li class="sep"></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                    <li class="sep"></li>
                                    <li><a href="/byb/contact.jsp">Contact</a></li>
                                </ul>
                                <a id="btn-toogle-menu" class="btn-toogle-menu" href="#alternate-menu">
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                </a>
                                <div id="right-menu">
                                    <ul class="alternate-menu">
                                        <li><a href="/byb/index.jsp">Home</a></li>
                                        <li><a href="/byb/how-it-work.jsp">Help</a></li>
                                        <li><a href="/byb/contact.jsp">Contact us</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="top-message clearfix">
                                <i class="icon iFolder"></i>
                                <span class="txt-message">Welcome to ByBo site.</span>
                                <div class="clear"></div>
                            </div>
                            <i id="sys_btn_toggle_search" class="icon iBtnRed make-right"></i>
                        </div>
                    </div>
                </div><!-- end: .wrap-top-menu -->
                <div class="container_12 clearfix">
                    <div class="grid_12 header-content">
                        <div id="sys_header_right" class="header-right">
                            <div class="account-panel">

                                <%if (session.getAttribute("user") != null) {%>
                                <a href="/byb/LoginServlet" class="btn btn-red ">My Bybo</a>
                                <%} else {%>
                                <a href="#" class="btn btn-red sys_show_popup_login">Register</a>
                                <a href="#" class="btn btn-black sys_show_popup_login">Login</a>
                                <%}%>
                            </div>
                            <div class="form-search">
                                <form action="#">
                                    <label for="sys_txt_keyword">
                                        <input id="sys_txt_keyword" class="txt-keyword" type="text" placeholder="Search projects"/>
                                    </label>
                                    <button class="btn-search" type="reset"><i class="icon iMagnifier"></i></button>
                                    <button class="btn-reset-keyword" type="reset"><i class="icon iXHover"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="header-left">
                            <h1 id="logo">
                                <a href="/byb/index.jsp"><img src="images/logo.png" alt="$SITE_NAME"/></a>
                            </h1>
                            <div class="main-nav clearfix">
                                <div class="nav-item">
                                    <a href="/byb/ProjectListServlet" class="nav-title">Discover</a>
                                    <p class="rs nav-description">Great Projects</p>
                                </div>
                                <span class="sep"></span>
                                <div class="nav-item">
                                    <a href="/byb/AddProjectServlet" class="nav-title">Start</a>
                                    <p class="rs nav-description">Your Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header><!--end: #header -->

            <div class="layout-2cols">
                <div class="content grid_8">
                    <div class="project-detail">
                        <div class="project-tab-detail tabbable accordion">
                            <ul class="nav nav-tabs clearfix">
                                <li class="active"><a href="#">Profile</a></li>
                                <li class="disable"><a href="#" class="be-fc-orange">Notifications</a></li>
                                <li><a href="#" class="be-fc-orange">My Comments</a></li>
                                <li><a href="#" class="be-fc-orange">Projects</a></li>
                            </ul>
                            <div class="tab-content">
                                <div>
                                    <h3 class="rs alternate-tab accordion-label">Profile</h3>
                                    <div class="tab-pane accordion-content active">
                                        <div class="form form-profile">
                                            <form action="/byb/UpdateUserServlet" method="post">
                                                <div class="row-item clearfix">
                                                    <label class="lbl" for="txt_firstname">First Name:</label>
                                                    <div class="val">
                                                        <input class="txt" type="text" id="txt_firstname" name="firstname" value="<%=user.getFirstname()%>">
                                                        <p class="rs description-input">First Name</p>
                                                    </div>
                                                </div>

                                                <div class="row-item clearfix">
                                                    <label class="lbl" for="txt_lastname">Last Name:</label>
                                                    <div class="val">
                                                        <input class="txt" type="text" id="txt_lastname" name="lastname" value="<%=user.getLastname()%>">
                                                        <p class="rs description-input">Last Name</p>
                                                    </div>
                                                </div>

                                                <div class="row-item clearfix">
                                                    <label class="lbl" for="txt_location">Location:</label>
                                                    <div class="val">
                                                        <input class="txt" type="text" id="txt_location" name="location" value="<%=user.getLocation()%>">
                                                    </div>
                                                </div>

                                                <div class="row-item clearfix">
                                                    <label class="lbl" for="txt_mail">Email : </label>
                                                    <div class="val">
                                                        <input class="txt" type="text" id="txt_mail" name="email" value="<%=user.getEmail()%>">
                                                    </div>
                                                </div>
                                                <div class="row-item clearfix">
                                                    <label class="lbl" for="biography">Biography:</label>
                                                    <div class="val">
                                                        <textarea class="txt fill-width" name="biography" id="biography" cols="30" rows="10" ><%=user.getBiography()%></textarea>
                                                        <p class="rs description-input">Short description</p>
                                                    </div>
                                                </div>

                                                <center>

                                                    <p class="wrap-btn-submit rs ta-r">
                                                        <input type="submit" class="btn btn-red btn-submit-all" value="Save all settings">
                                                    </p></center>
                                            </form>
                                        </div>
                                    </div><!--end: .tab-pane -->
                                </div>
                                <div>
                                    <h3 class="rs alternate-tab accordion-label">Notifications</h3>
                                    <div class="tab-pane accordion-content">
                                        <br /> Notifications<br /> <br /> <br /> <br /><br /><br />

                                    </div><!--end: .tab-pane -->
                                </div>




                                <div>
                                    <h3 class="rs alternate-tab accordion-label">My Comments</h3>
                                    <div class="tab-pane accordion-content">
                                        <div class="box-list-comment">
                                            
                                             <%
                                            if(comments!=null){
                                                for (Comment comment : comments) {

                                            %>
                                            <div class="media comment-item">
                                                <a href="#" class="thumb-left">
                                                    <img src="images/ex/th-90x90-1.jpg" alt="$TITLE">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="rs comment-author">
                                                        <a href="#" class="be-fc-orange fw-b">Project name : <%=comment.getProjectName()%></a>
                                                    </h4>
                                                    <p class="rs comment-content">><%=comment.getComment()%></p>
                                                    <p class="rs time-post">><%=comment.getComment_ts()%></p>
                                                </div>
                                            </div><!--end: .comment-item -->
                                            
                                                 <%}}%>

                                        </div>
                                    </div><!--end: .tab-pane -->
                                </div>
                                <div>
                                    <h3 class="rs alternate-tab accordion-label">Projects</h3>
                                    <div class="tab-pane accordion-content">


                                        <%
                                        if(userProjects!=null){
                                            for (Project project : userProjects) {%>

                                        <div class="box-marked-project project-short inside-tab">
                                            <div class="top-project-info">
                                                <div class="content-info-short clearfix">
                                                    <a href="#" class="thumb-img">
                                                        <img src="images/ex/th-292x204-1.jpg" alt="$TITLE">
                                                    </a>
                                                    <div class="wrap-short-detail">
                                                        <h3 class="rs acticle-title"><a class="be-fc-orange" href="/byb/ProjectServlet?projectid=<%=project.getProjectid()%>">Project Name : <%=project.getProjectname()%></a></h3>
                                                        <p class="rs tiny-desc"> <span class="fw-b fc-gray"><%=project.getLocation()%></span></p>
                                                        <p class="rs title-description"><%=project.getDescription()%></p>
                                                    </div>
                                                    <p class="rs clearfix comment-view">
                                                        <a href="#" class="fc-gray be-fc-orange">*** comments ToDo !!!</a>
                                                        <span class="sep">|</span>
                                                        <a href="#" class="fc-gray be-fc-orange">*** views ToDo !!!</a>
                                                    </p>
                                                </div>
                                            </div><!--end: .top-project-info -->

                                            <div class="bottom-project-info clearfix">
                                                <div class="project-progress sys_circle_progress" data-percent="<%=project.getPercentage()%>">
                                                    <div class="sys_holder_sector"></div>
                                                </div>
                                                <div class="group-fee clearfix">
                                                    <div class="fee-item">
                                                        <p class="rs lbl">Bankers</p>
                                                        <span class="val">
                                                        
                                                        <% if (project.getBackers()!=null && !project.getBackers().isEmpty()) { %> 
                                                        <%=project.getBackers().size()%>
                                                           <% } else { %>
                                                          <%= 0%>
                                                            <% } %>
                                                        
                                                        </span>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="fee-item">
                                                        <p class="rs lbl">Pledged</p>
                                                        <span class="val"><%=project.getPledge()%></span>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="fee-item">
                                                        <p class="rs lbl">Days Left</p>
                                                        <span class="val"><%=project.getDayLeft()%></span>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div><!--end: .box-marked-project -->

                                        <% }}%>

                                    </div><!--end: .tab-pane -->
                                </div>
                            </div>
                        </div><!--end: .project-tab-detail -->
                    </div>
                </div><!--end: .content -->
                <div class="sidebar grid_4">
                    <div class="box-gray project-author">
                        <h3 class="title-box"><%=user.getUsername()%></h3>
                        <div class="media">
                            <a href="#" class="thumb-left"> 
                                <img src="images/ex/th-90x90-1.jpg" alt="$USER_NAME"/>
                            </a>
                            <div class="media-body">
                                <h4 class="rs pb10"><a href="#" class="be-fc-orange fw-b"><%=user.getFirstname() + " " + user.getLastname()%> </a></h4>
                                <p class="rs"><%=user.getLocation()%></p>
                            </div>
                        </div>
                        <div class="author-action">
                            <a class="btn btn-red" href="/byb/addProject.jsp">Start new project</a>
                            <a class="btn btn-white" href="/byb/disconnect.jsp">Logout</a>
                        </div>
                    </div><!--end: .project-author -->
                </div><!--end: .sidebar -->
                <div class="clear"></div>
            </div>


            <footer id="footer">

                <div class="copyright">
                    <div class="container_12">
                        <div class="grid_12">
                            <a class="logo-footer"><img src="images/logo-2.png" alt="$SITE_NAME"/></a>
                            <p class="rs term-privacy">
                                <a class="fw-b be-fc-orange" >Tunisia</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >INSAT</a>
                                <span class="sep">/</span>
                                <a class="fw-b be-fc-orange" >GL4</a>
                            </p>
                            <p class="rs ta-c fc-gray-dark site-copyright">By  Anis BARKAOUI  &   Mohamed AFDHAL </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </footer>

        </div>

        <div class="popup-common" id="sys_popup_common">
            <div class="overlay-bl-bg"></div>
            <div class="container_12 pop-content">
                <div class="grid_12 wrap-btn-close ta-r">
                    <i class="icon iBigX closePopup"></i>
                </div>
                <div class="grid_6 prefix_1">
                    <div class="form login-form">
                        <form action="#">
                            <h3 class="rs title-form">Register</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">New to Kickstars?</h4>
                                <p class="rs">A Kickstars account is required to continue.</p>
                                <div class="form-action">
                                    <label for="txt_name">
                                        <input id="txt_name" class="txt fill-width" type="text" placeholder="Enter full name"/>
                                    </label>
                                    <div class="wrap-2col clearfix">
                                        <div class="col">
                                            <label for="txt_email">
                                                <input id="txt_email" class="txt fill-width" type="email" placeholder="Enter your e-mail address"/>
                                            </label>
                                            <label for="txt_re_email">
                                                <input id="txt_re_email" class="txt fill-width" type="email" placeholder="Re-enter your e-mail adress"/>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label for="txt_password">
                                                <input id="txt_password" class="txt fill-width" type="password" placeholder="Enter password"/>
                                            </label>
                                            <label for="txt_re_password">
                                                <input id="txt_re_password" class="txt fill-width" type="password" placeholder="Re-enter password"/>
                                            </label>
                                        </div>
                                    </div>
                                    <p class="rs pb10">By signing up, you agree to our <a href="#" class="fc-orange">terms of use</a> and <a href="#" class="fc-orange">privacy policy</a>.</p>
                                    <p class="rs ta-c">
                                        <button class="btn btn-red btn-submit" type="submit">Register</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="form login-form">
                        <form action="#">
                            <h3 class="rs title-form">Login</h3>
                            <div class="box-white">
                                <h4 class="rs title-box">Already Have an Account?</h4>
                                <p class="rs">Please log in to continue.</p>
                                <div class="form-action">
                                    <label for="txt_email_login">
                                        <input id="txt_email_login" class="txt fill-width" type="email" placeholder="Enter your e-mail address"/>
                                    </label>
                                    <label for="txt_password_login">
                                        <input id="txt_password_login" class="txt fill-width" type="password" placeholder="Enter password"/>
                                    </label>

                                    <label for="chk_remember" class="rs pb20 clearfix">
                                        <input id="chk_remember" type="checkbox" class="chk-remember"/>
                                        <span class="lbl-remember">Remember me</span>
                                    </label>
                                    <p class="rs ta-c pb10">
                                        <button class="btn btn-red btn-submit" type="submit">Login</button>
                                    </p>
                                    <p class="rs ta-c">
                                        <a href="#" class="fc-orange">I forgot my password</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-20585382-5', 'megadrupal.com');
            ga('send', 'pageview');

        </script>
    </body>

</html>