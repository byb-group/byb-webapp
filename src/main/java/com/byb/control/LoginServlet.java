package com.byb.control;

import com.byb.model.Comment;
import com.byb.model.Project;
import com.byb.model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import com.byb.service.*;
import java.util.ArrayList;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    UserService us;
    ProjectService ps;
    CommentService cs;

    public LoginServlet() {
        us = new UserService();
        ps = new ProjectService();
        cs = new CommentService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            response.sendRedirect("/byb/profile.jsp");
        } else {
            response.sendRedirect("/byb/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        if (session.getAttribute("user") == null) {

            User u = us.login(request.getParameter("login"), request.getParameter("password"));

            if (u != null) {
                ArrayList<Project> userProjects = (ArrayList<Project>) ps.getProjectByUsername(u.getUsername());
                ArrayList<Comment> comments = (ArrayList<Comment>) cs.getCommentByUsername(u.getUsername());

                session.setAttribute("user", u);
                session.setAttribute("userProjects", userProjects);
                session.setAttribute("comments", comments);

                getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
            } else {
                getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);

            }
        } else {
            getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
        }

    }
}