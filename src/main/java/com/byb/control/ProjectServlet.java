package com.byb.control;

import com.byb.model.Comment;
import com.byb.model.Project;
import com.byb.model.User;
import com.byb.service.CommentService;
import com.byb.service.ProjectService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ProjectServlet", urlPatterns = {"/ProjectServlet"})
public class ProjectServlet extends HttpServlet {

    ProjectService ps;
    CommentService cs;

    public ProjectServlet() {
        ps = new ProjectService();
        cs= new CommentService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String p = request.getParameter("projectid");
        HttpSession session = request.getSession();
        ArrayList<Comment> comments = new ArrayList<Comment>();

        Project project = null;
        if (p == null) {
            response.sendRedirect("/byb/ProjectListServlet");
        } else {
             project = ps.getProjectById(p);
             comments= (ArrayList<Comment>) cs.getCommentByProjectId(project.getProjectid());
             
            }

        session.setAttribute("project", project);
        session.setAttribute("comments", comments);
        getServletContext().getRequestDispatcher("/project.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int amount = Integer.parseInt(request.getParameter("amount"));
        int reward = Integer.parseInt(request.getParameter("reward"));
        HttpSession session = request.getSession();
        
        
        Project project = (Project) session.getAttribute("project");
        
        User user = (User) session.getAttribute("user");
        
        if (project == null) {
            response.sendRedirect("/byb/ProjectListServlet");
        } else {
            int predAmount = project.getBackers().get(user.getUsername());
             ps.backProject(project.getProjectid(),user.getUsername(),predAmount+amount,reward);
             project.addBacker(user.getUsername(),amount);
            }

        session.setAttribute("project", project);
        getServletContext().getRequestDispatcher("/project.jsp").forward(request, response);
    }
}