package Controles;

import com.byb.model.Comment;
import com.byb.model.Project;
import com.byb.model.User;
import com.byb.service.CommentService;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AddCommentServlet", urlPatterns = {"/AddCommentServlet"})
public class AddCommentServlet extends HttpServlet {

    CommentService cs;

    public AddCommentServlet() {
        cs = new CommentService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            response.sendRedirect("/byb/profile.jsp");
        } else {
            response.sendRedirect("/byb/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Project project = (Project) session.getAttribute("project");
        ArrayList<Comment> comments = (ArrayList<Comment>) session.getAttribute("comments");
        
        if (user != null) {
            
            Comment comment = new Comment();
            comment.setProjectid(project.getProjectid());
            comment.setUsername(user.getUsername());

            //To get text from text area
            StringBuffer text = new StringBuffer(request.getParameter("comment"));
            int loc = (new String(text)).indexOf('\n');
            while (loc > 0) {
                text.replace(loc, loc + 1, "<BR>");
                loc = (new String(text)).indexOf('\n');
            }
            project.setDescription(text.toString());
            
            comment.setComment(text.toString());

            comment = cs.addComment(comment);
            
            if (comment != null) {
                comments.add(comment);
                response.sendRedirect("/byb/ProjectServlet?projectid="+project.getProjectid().toString());
            } else {
                getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);

            }
        } else {
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
