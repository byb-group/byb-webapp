package Controles;

import com.byb.model.Project;
import com.byb.service.ProjectService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ProjectListServlet", urlPatterns = {"/ProjectListServlet"})
public class ProjectListServlet extends HttpServlet {

    ProjectService ps;

    public ProjectListServlet() {
        ps = new ProjectService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String cat = request.getParameter("category");
        HttpSession session = request.getSession();

        ArrayList<Project> projects = null;
        if (cat == null) {
            //Get ALL project 
            projects = (ArrayList<Project>) ps.getAllProjects();
            cat = "ALL Projects";
        } else {
             projects = (ArrayList<Project>) ps.getProjectByCategory(cat);
            }

        session.setAttribute("projects", projects);
        session.setAttribute("category", cat);
        getServletContext().getRequestDispatcher("/projects.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}