package Controles;

import com.byb.model.User;
import com.byb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {

    UserService us;

    public RegisterServlet() {
        us = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            response.sendRedirect("/byb/profile.jsp");
        } else {
            response.sendRedirect("/byb/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            User user = new User();
            user.setUsername(request.getParameter("username"));
            user.setEmail(request.getParameter("email"));
            user.setPassword(request.getParameter("password"));


            String result = us.addUser(user);
            if (result.matches("Done")) {
                session.setAttribute("user", user);
                getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
            } else {
                getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);

            }
        } else {
            getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
        }
    }
}
