package Controles;

import com.byb.model.User;
import com.byb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UpdateUserServlet", urlPatterns = {"/UpdateUserServlet"})
public class UpdateUserServlet extends HttpServlet {

    UserService us;

    public UpdateUserServlet() {
        us = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            response.sendRedirect("/byb/profile.jsp");
        } else {
            response.sendRedirect("/byb/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        //User already logged in
        User predUser = (User) session.getAttribute("user");
        if (predUser != null) {
            User user = new User();
            user.setUsername(predUser.getUsername());
            user.setPassword(predUser.getPassword());
            user.setFirstname(request.getParameter("firstname"));
            user.setLastname(request.getParameter("lastname"));
            user.setLocation(request.getParameter("location"));
            user.setEmail(request.getParameter("email"));

            //To get text from text area
            StringBuffer text = new StringBuffer(request.getParameter("biography"));
            int loc = (new String(text)).indexOf('\n');
            while (loc > 0) {
                text.replace(loc, loc + 1, "<BR>");
                loc = (new String(text)).indexOf('\n');
            }
           user.setBiography(text.toString());


            String result = us.updateUser(user);
            if (result.matches("Done")) {
                session.setAttribute("user", user);
                getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
            } else {
                getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);

            }
        } else {
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
