package Controles;

import com.byb.model.Project;
import com.byb.model.User;
import com.byb.service.ProjectService;
import com.byb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AddProjectServlet", urlPatterns = {"/AddProjectServlet"})
public class AddProjectServlet extends HttpServlet {

    ProjectService ps;

    public AddProjectServlet() {
        ps = new ProjectService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            response.sendRedirect("/byb/profile.jsp");
        } else {
            response.sendRedirect("/byb/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            Project project = new Project();
            project.setProjectname(request.getParameter("projectName"));

            project.setUsername(user.getUsername());

            project.setLocation(request.getParameter("location"));

            //To get text from text area
            StringBuffer text = new StringBuffer(request.getParameter("description"));
            int loc = (new String(text)).indexOf('\n');
            while (loc > 0) {
                text.replace(loc, loc + 1, "<BR>");
                loc = (new String(text)).indexOf('\n');
            }
            project.setDescription(text.toString());

            project.setCategory(request.getParameter("category"));
            project.setBudjet(Integer.parseInt(request.getParameter("budjet")));
            project.setMinimum_pledge(Integer.parseInt(request.getParameter("minimum_pledge")));

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            try {
                project.setFunding_end_date(formatter.parse(request.getParameter("funding_end_date")));
            } catch (ParseException ex) {
            }


            project = ps.addProject(project);
            if (project != null) {
                session.setAttribute("project", project);
                ArrayList<Project> userProjects =(ArrayList<Project>)session.getAttribute("userProjects");
                userProjects.add(project);
                response.sendRedirect("/byb/ProjectServlet?projectid="+project.getProjectid().toString());
            } else {
                getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);

            }
        } else {
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
